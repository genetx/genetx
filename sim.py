import os
import subprocess
import time
import sys
output_file = "population.cpp"
def compile():
    print "Compiling...",
    sys.stdout.flush()
    stime=time.time()
    ret=os.system("g++ -std=c++11 -g -o population -Wall population.cpp -O0")
    deltatime=time.time()-stime
    print("%.3fseconds" % (round(deltatime,3)))

    if(ret !=0):
        print "compile did not succeed, quitting"
        exit(0)
#executed the most recently compiled population. returns a list of fitnesses
def execute(num_fns):
    fitness=[]
    for i in range(0,num_fns):
        proc = subprocess.Popen(['./population',"%s" % i],stdout=subprocess.PIPE)
        ret=float(proc.stdout.readline())
        #print "Individual %s succeeded @%s" % (i,ret)
        fitness.append(ret)
    return fitness
