from deap import base
from deap import tools, creator
from scoop import futures
import random


# LAUNCH python -m scoop --hostfile hosts testScript.py to run test script on 

SELECT_PERCENT_FROM_POP = 30
NGEN = 40
NLINES = 300
RANGE_FLOATS = 300.0 #0-300 is the value of each float.
CXPB = 0.2
MUTPB = 0.1
#register toolbox.map to be futures.map
'''
doEvaluate func( some index){
    run the test on shell script on (some index)th function. grab output.
    return output fitness value.
}
'''
def evaluateInd(i): #test the i-th function.
    #Do some computation    run the test on shell script on (some index)th function. grab output.
    #return output fitness value.
    return random.random(),

def compileCode(pop):
    return "OK"

def r_floatconst():
    return random.random()*RANGE_FLOATS

def generateRandomInd():
    headers = [tools.initRepeat(list, r_floatconst,256)]
    for x in range(NLINES):
        headers.append(tools.initCycle(list, 
            [lambda: random.randrange(16), 
            lambda: random.randrange(2),
            lambda: random.randrange(256),
            lambda: random.randrange(2),
            lambda: random.randrange(256),
            lambda: random.randrange(4),
            lambda: random.randrange(256)], n=1))
    return headers



if __name__ == '__main__':
    toolbox = base.Toolbox()

    hall_of_fame = tools.HallOfFame(20)

    creator.create("FitnessMax", base.Fitness, weights=(1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMax)
    
    toolbox.register("map", futures.map)
    toolbox.register("compile", compileCode)
    toolbox.register("mate", tools.cxUniform, indpb=0.2)
    toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=1, indpb=0.2)
    toolbox.register("select", tools.selTournament, tournsize=3)
    toolbox.register("evaluate", evaluateInd)

    toolbox.register("individual", tools.initRepeat, creator.Individual, generateRandomInd, n=1)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    pop = toolbox.population(n=100)

    fitnesses = toolbox.map(toolbox.evaluate, range(len(pop)))
    for ind, fit in zip(pop, fitnesses):
        ind.fitness.values = fit

    for g in range(NGEN):
        # Select the next generation individuals
        offspring = toolbox.select(pop, len(pop)*SELECT_PERCENT_FROM_POP/100)
        # Clone the selected individuals
        offspring = map(toolbox.clone, offspring)

        # Apply crossover on the offspring
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < CXPB:
                toolbox.mate(child1, child2)
                del child1.fitness.values
                del child2.fitness.values

        # Apply mutation on the offspring
        for mutant in offspring:
            if random.random() < MUTPB:
                toolbox.mutate(mutant)
                del mutant.fitness.values

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]

        # compile invalid_ind function into C code
        toolbox.compile(invalid_ind)

        # make sure this is transferred to all the machines.
        fitnesses = toolbox.map(toolbox.evaluate, range(len(invalid_ind)))

        # fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # The population is entirely replaced by the offspring
        pop[:] = offspring
        print g
