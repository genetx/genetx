//includes
#include <functional>
#include <stdint.h>
#include <string>
#include <cmath>
using namespace std;

//config
typedef int32_t signed_t;
typedef uint32_t unsigned_t;
#define DATA_SIZE 255
#define INPUT_START DATA_SIZE
#define INPUT_SIZE 1
unsigned_t memory[DATA_SIZE+INPUT_SIZE];
//int remaining_times;
#define TOTAL_TIMES 1024
//PP
#define do_times(times,fn) do_(times,remaining_times,[&remaining_times]() mutable->unsigned_t{ return (fn); })
#define add(a,b)    ((a)+(b))
#define sub(a,b)    ((a)-(b))
#define mul(a,b)    ((a)*(b))
#define div(a,b)    ((a)/(b))
#define lshift(a,b) ((a)<<((b)%32))
#define rshift(a,b) ((a)>>((b)%32))
#define or_(a,b)    ((a)|(b))
#define and_(a,b)   ((a)&(b))
#define not_(a)     (~(a))
#define xor_(a,b)   ((a)^(b))
#define mod(a,b)    (((b)==0)?(a):(a)%(b))
#define if_then_else(a,b,c) ((a)?(b):(c))
#define if_eq_then_else(a,b,c,d) (((a)==(b))?(c):(d))
#define if_lt_then_else(a,b,c,d) (((a)<(b))?(c):(d))
#define if_lteq_then_else(a,b,c,d) (((a)<=(b))?(c):(d))
#define if_signed_lt_then_else(a,b,c,d) ((((signed_t)(a))<((signed_t)(b)))?(c):(d))
#define if_signed_lteq_then_else(a,b,c,d) ((((signed_t)(a))<=((signed_t)(b)))?(c):(d))
#define load(a) (memory[a])
#define store(a,v) (memory[a]=(v))
#define input0 (memory[INPUT_START])
//helper functions
unsigned_t do_(unsigned_t times, unsigned_t& remaining_times,std::function<unsigned_t()> f)
{
    unsigned_t ret=0;
    times=remaining_times>=times?times:remaining_times;
    //always allow one time
    remaining_times-=times;
    if(remaining_times==0)
        remaining_times++;

    for(unsigned_t i=0;i<times;i++)
    {
        ret=f();
    }
    return ret;
}
void initialize();
int execute();
inline float error_f(float set, float actual)
{
    return abs(set-actual);
}
int main(int argc, char *argv[])
{
    if(argc!=2)
    {
        printf("Usage: %s <function#>\n",argv[0]);
        return 0;
    }
    int choice=stoi(argv[1]);
    if(choice>=NUM_FUNCTIONS)
    {
        printf("function#==%d >= NUM_FUNCTIONS==%d\n",choice,NUM_FUNCTIONS);
        return 0;
    }
    initialize();
    float total_error=0;
    float vout_set=500;
    float vout=0;
    float integratetime=20;
    // for(int i=0;i<128*128;i++)
    // {
    //     int set =i%128;
    //     input0=set/2;
    //     set=input0*2;
    //     int actual=functions[choice]()%128;
    //     total_error+=error_f(set,actual);
    // }

    //this is a 1st order system. numbers above 500 will increase the
    //output a little bit. below, the opposite.
    for(int i=0;i<128*128;i++)
    {
        input0=vout+vout_set;
        int actual=functions[choice]();
        if(actual>2*vout_set)
            actual=2*vout_set;
        vout+=((float)actual-vout_set)/integratetime;
        //printf("vout=%f, actual=%d\n",vout,actual);
        total_error+=error_f(vout_set,vout);
    }
    printf("%f\n",total_error);
    return 0;
}
void initialize()
{
    memset(memory,sizeof(memory),0);
}
/*int execute()
{
    remaining_times=TOTAL_TIMES;
    //insert genetic code here!
    return 14;
}*/