import operator
import sys
import random
from deap import base, creator, gp, tools

pset = gp.PrimitiveSetTyped("MAIN", [int], int)

# binops
pset.addPrimitive(operator.add, [int, int], int)
pset.addPrimitive(operator.sub, [int, int], int)
pset.addPrimitive(operator.mul, [int, int], int)
pset.addPrimitive(operator.lshift, [int, int], int)
pset.addPrimitive(operator.rshift, [int, int], int)
pset.addPrimitive(operator.mod, [int, int], int)

pset.addPrimitive(operator.or_, [int, int], int)
pset.addPrimitive(operator.and_, [int, int], int)
pset.addPrimitive(operator.not_, [int], int)
pset.addPrimitive(operator.xor, [int, int], int, 'xor_')

# conditionals
def if_then_else(input, output1, output2):
    return output1 if input else output2

def if_eq_then_else(input1, input2, output1, output2):
    return output1 if input1 == input2 else output2

def if_lt_then_else(input1, input2, output1, output2):
    return output1 if input1 < input2 else output2

def if_lteq_then_else(input1, input2, output1, output2):
    return output1 if input1 <= input2 else output2

def if_signed_lt_then_else(input1, input2, output1, output2):
    return output1 if input1 < input2 else output2

def if_signed_lteq_then_else(input1, input2, output1, output2):
    return output1 if input1 <= input2 else output2

pset.addPrimitive(if_then_else, [int, int, int], int)
pset.addPrimitive(if_eq_then_else, [int, int, int, int], int)
pset.addPrimitive(if_lt_then_else, [int, int, int, int], int)
pset.addPrimitive(if_lteq_then_else, [int, int, int, int], int)
pset.addPrimitive(if_signed_lt_then_else, [int, int, int, int], int)
pset.addPrimitive(if_signed_lteq_then_else, [int, int, int, int], int)

# loop
def do_times(times, action):
    for i in range(1, times if times < 1000 else 1000):
        action()
    return action()

pset.addPrimitive(do_times, [int, int], int)

# memory
MEMORY_SIZE = 1

memory = [0]*MEMORY_SIZE # this is just to make the functions act as like a spec
def load(address):
    return memory[address]

def store(address, value):
    memory[address] = value
    return value

pset.addPrimitive(load, [float], int)
pset.addPrimitive(store, [float, int], int)

# address
pset.addEphemeralConstant('address', lambda: random.randint(0, 255), float)

class UnsignedInt:
    num = 0
    def __init__(self, num):
        self.num = num
    def __repr__(self):
        return '((unsigned_t)' + str(self.num) + ')'

pset.addEphemeralConstant('constant', lambda: UnsignedInt(long(random.expovariate(1.0/70.0))), int)

pset.renameArguments(ARG0='input0')

