/*.......1.........2.........3.........4.........5.........6.........7.........8


/*=== CM_gene ROUTINE ===*/
#define PWM_FREQ 50000
#define CONTROLLER_FREQ 50000
#define V_SET 7 
#define V_PWM 20
#define DELTA_PWM (0.05) 

#define DATA_SIZE 256
#define CONST_SIZE 256

#ifndef GENERATION_C
    #define GENERATION_C
    #include "generation.c"
//function_table[POP_SIZE]
//POP_SIZE
#endif


typedef struct {
    void (*gen_fn)(float*);
    float data[DATA_SIZE];
    double pwm_cycle_start;
    double pwm_period;
    double controller_cycle_next;
    double controller_period;
    double vout; //For PWM
    //float constants[CONST_SIZE];
} controller_data_t;
double pwm_control(controller_data_t* d, double sim_time);
void gene_control(controller_data_t* d, double v, double sim_time);
void genefn(controller_data_t* d, double v, double sim_time);


double pwm_control(controller_data_t* d, double sim_time)
{
    if(sim_time>=(d->pwm_cycle_start+d->pwm_period))
    {
        d->pwm_cycle_start+=d->pwm_period;
        d->curr_pwm=d->next_pwm;
    }
    double end_time=d->pwm_cycle_start+d->pwm_period*d->curr_pwm;
    
    double vchoice=(end_time>sim_time)?d->vout:0;

    return vchoice;
}
void genefn(controller_data_t* d, double v, double sim_time)
{
    double error=d->vset-v;
    
    double dt=sim_time-d->gene.previous_time;
    d->gene.previous_time=sim_time;
    d->gene.integral+=error*dt;
    double derivative=(error-d->gene.previous_error)/dt;
    d->next_pwm=Kp*error+Ki*d->gene.integral+(Kd)*derivative;

    if(d->next_pwm>1)
    {
        d->next_pwm=1;
    }else if(d->next_pwm<0)
    {
        d->next_pwm=0;
    }
}
void gene_control(controller_data_t* d, double v, double sim_time)
{
    if(sim_time>=d->controller_cycle_next)
    {
        d->controller_cycle_next+=d->controller_period;
        genefn(d,v,sim_time);
    }
}

void cm_gene
(ARGS)   /* structure holding parms, inputs, outputs, etc.     */
{

    if(INIT)
    {
        //init data
        cm_analog_alloc(0, sizeof(controller_data_t));
        controller_data_t* d = 
        (controller_data_t *) cm_analog_get_ptr(0, 0);
        memset(d->data,0,sizeof(float * DATA_SIZE));
        int fnumber = (int)((PARAM(fn_num)));
        assert(fnumber<POP_SIZE);
        d->gen_fn=function_table[fnumber];
        d->pwm_cycle_start=TIME;
        d->pwm_period=1.0/((double)PWM_FREQ);
        d->curr_pwm=0;
        d->next_pwm=0;
        d->vset=V_SET;
        d->controller_cycle_next=TIME;
        d->controller_period=1.0/((double)CONTROLLER_FREQ);
        d->vout=V_PWM;
    }else{
        //run sim
        controller_data_t* d = 
            (controller_data_t *) cm_analog_get_ptr(0, 0);

        double v=INPUT(in);
        OUTPUT(out)=pwm_control(d,TIME);

        gene_control(d,v,TIME);
        OUTPUT(pwm_out)=d->curr_pwm;


    }

}
