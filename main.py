import operator
import sys
import random
import math
import numpy
from deap import base, creator, gp, tools, algorithms
from evolve import pset

import write_c
import sim

# do it ourselves
# some ripped off from http://colinfdrake.com/2011/05/01/ga-in-python.html
def weighted_choice(items):
    """
    Chooses a random element from items, where items is a list of tuples in
    the form (item, weight). weight determines the probability of choosing its
    respective item. Note: this function is borrowed from ActiveState Recipes.
    """
    weight_total = sum((item[1] for item in items))
    n = random.uniform(0, weight_total)
    for item, weight in items:
        if n < weight:
            return item
        n = n - weight
    return item

# returns list of fitnesses
def evaluate_pop(pop):
    # evaluate all
    write_c.population_to_c(pop)
    sim.compile()
    fitnesses = sim.execute(len(pop))
    
    for p in range(len(pop)):
        fitnesses[p] = fitnesses[p] + len(pop[p])*20
    print fitnesses
    return fitnesses

import pygraphviz as pgv
def graph(ind, num):
    nodes, edges, labels = gp.graph(ind);
    g = pgv.AGraph()
    g.add_nodes_from(nodes);
    g.add_edges_from(edges);
    g.layout(prog="dot")
    for i in nodes:
        n = g.get_node(i);
        n.attr["label"] = str(labels[i]).replace('((unsigned_t)', '').replace(')', '')

    g.draw("tree"+str(num)+".pdf");


# stats_fit = tools.Statistics(lambda ind: ind.fitness.values)
# stats_size = tools.Statistics(len)
# mstats = tools.MultiStatistics(fitness=stats_fit, size=stats_size)
# mstats.register("avg", numpy.mean)
# mstats.register("std", numpy.std)
# mstats.register("min", numpy.min)
# mstats.register("max", numpy.max)

# pop = toolbox.population(n=300)
# hof = tools.HallOfFame(1)
# pop, log = algorithms.eaSimple(pop, toolbox, 0.5, 0.1, 40, stats=mstats,
#                                halloffame=hof, verbose=True)

if __name__ == '__main__':
    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMin)

    toolbox = base.Toolbox()
    toolbox.register("expr", gp.genHalfAndHalf, pset=pset, min_=1, max_=2, type_=int)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)

    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("compile", gp.compile, pset=pset)

    toolbox.register("select", tools.selTournament, tournsize=3)
    toolbox.register("mate", gp.cxOnePoint)
    toolbox.register("expr_mut", gp.genFull, min_=0, max_=2)
    toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=pset)

    POPULATION = 10
    GEN_NUM = 300

    pop = toolbox.population(n=POPULATION)
    pop[0] = gp.PrimitiveTree.from_string('add(mul(sub(1000, input0), 20), 500)', pset)
    for generation in range(1, GEN_NUM):

        for i in range(0, len(pop)):
            graph(pop[i], '%i-%i' % (generation, i))

        print generation
        fitnesses = evaluate_pop(pop)

        weighted_pop = []
        largest = (None, 0)
        for i in range(0, len(pop)):
            expr = pop[i]
            fitness_val = fitnesses[i]

            if fitness_val == 0:
                pair = (expr, 1.0)
            else:
                pair = (expr, 1.0/(fitness_val+1))
            
            if pair[1] > largest[1]:
                largest = pair

            weighted_pop.append(pair)

        pop = [largest[0]]

        if random.random() > 0.8:
            #20% chance.
            pop.append(toolbox.individual()); 

        # pprint.pprint(weighted_pop)

        for _ in xrange(POPULATION/2 - len(pop)):
            ind1 = toolbox.clone(weighted_choice(weighted_pop))
            ind2 = toolbox.clone(weighted_choice(weighted_pop))
            # print ind1;
            # print ind2;

            if ind1 != ind2:
                ind1, ind2 = toolbox.mate(ind1, ind2)
            # print "------------------"
            # print ind1;
            # print ind2;
            # print "=================="

            # graph(ind1,2);
            # graph(ind2,3);

            pop.append(toolbox.mutate(ind1)[0])
            pop.append(toolbox.mutate(ind2)[0])

    for expr in pop:
        print expr
