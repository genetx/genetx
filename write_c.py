output_file = "population.cpp"
base = "base.cpp"
def individual_to_c(num, str):
    s="\nint execute_%s(){\nunsigned_t remaining_times  __attribute__ ((unused)) =TOTAL_TIMES;\nreturn (%s);\n}\n" % (num, str)
    return s;
def gen_fn_arr(num):
    s="int (*functions[%s]) ()={\n"%num
    for i in range(0,num):
        s=s+"execute_%s,\n"%i
    s=s[:(len(s)-2)]
    s="%s\n};"%s
    return s
# call this function to write the C file
#call population_to_c, compile, execute
def population_to_c(stra):
    s=""
    for i in range(0,len(stra)):
        s=s+individual_to_c(i,stra[i])
    start="#define NUM_FUNCTIONS %s\nextern int (*functions[%s]) ();\n"%(len(stra),len(stra))
    base_file = open(base,'r');
    f= open(output_file, 'w');
    f.write(start)
    f.write(base_file.read())
    f.write(s)
    f.write(gen_fn_arr(len(stra)))
    f.close()
#t=["add(7,2)","add(8,2)","do_times(3,do_times(2,7+5))","do_times(2,store(1,add(load(1),3)))"]
#population_to_c(t)