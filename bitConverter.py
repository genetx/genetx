import sys
import os
import struct

HEADER_BIT_COUNT = 8192 #Includes constants
HEADER_PIECES = 32 #number of bits in each num in the header
BITS_PER_INSTR = 33
MAX_ITER_ALLOWED = 1000 # number of branches maximum

#Need to cast float double pointer to int pointer for bitwise shit
#Use python string replace in implementation --> .format(0, 1, ...)
ops = ['noop',
'{2}={0}-{1}',
'{2}={0}*{1}',
'{2}={0}/{1}',
'{2}={0}+{1}',
'{2}={0}<<{1}',
'{2}={0}>>{1}',
'{2}={0}|{1}',
'{2}={0}&{1}',
'{2}={0}~{1}',
'{2}={0}^{1}',
'if ({0}=={1} && --itc > 0){ goto {2}; }',
'if ({0}!={1} && --itc > 0){ goto {2}; }',
'if ({0}>{1} && --itc > 0){ goto {2}; }',
'if (--itc > 0){ goto {2}; }']
# Instruction: <opcode><pA=const><pA><pB=const><pB><junk><dest>
# sizes:          <5>     <1>     <8>     <1>  <8>  <2>   <8>



# [[5,2][1][45][0][31][3][153]]
def bits_to_int(str):
	if len(str)!=32:
		print "NOT 32 BITS!"
	i = int(str, 2)
	return struct.unpack('u', struct.pack('I', i))[0]

def bits_to_instr(str):


def convert(bits, header, func_number):
	f = open('generator.c', 'w')

	#write function start
	f.write("""void f{0}(double* da){
			int itc =""" + str(MAX_ITER_ALLOWED)+";")

	f.write("float fa[] = {"+"}")

	lineLabel = 1

	constants=[]
	for i in range(0,HEADER_BIT_COUNT,HEADER_PIECES):
		h=header[i:i+HEADER_PIECES]
		constants.append(bits_to_int(h))

	if len(bits)%BITS_PER_INSTR !=
	for i in range(0,len(bits),BITS_PER_INSTR):
	# instructions = [bits[i:i+BITS_PER_INSTR] for i in range(HEADER_BIT_COUNT + 1, len(bits), BITS_PER_INSTR)]
	for b, index in enumerate(bits[0]):
		# opcode = convertToInt(i[:5])
		# paConst = convertToInt(i[5:6])
		# pa = convertToInt(i[6:14])
		# pbConst = convertToInt(i[14:15])
		# pb = convertToInt(i[15:23])
		# dest = convertToInt(i[25:])
		# if opcode == 0:
		# 	continue
		# op = ops[opcode]
		if bits[0][index] == 0:
			continue


	f.write("}")


		

	f.close()

def convertToInt(bits):
	return int(bits, base=2)

def main():
    testBits = '000011010100101000101111011111'
    convert(testBits)

if __name__ == '__main__':
    main()